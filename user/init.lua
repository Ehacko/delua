local wibox = require("wibox")
local beautiful = require("beautiful")
User = {}

User.startup = {
    "picom"
}

-- Choosen theme name
User.theme = "theme_name"

-- Apps list
User.apps = {}


User.panels = function (beautiful, s, wibox)

    -- Describe top panels
    local top_panel = {
        params = {
            position = "top",
            height = 20,
            bg = "transparent",
            -- size = 20, 
            -- size_activator = 1
            -- show_delay = 0.25,
            -- hide_delay = 0.5,
            -- easing = 2,
            -- delta = 1,

            screen = s
        },
        setup = {
            layout = wibox.layout.align.horizontal,
            expand = "none",
            { -- Left widgets
                layout = wibox.layout.fixed.horizontal,
                -- s.mylayoutbox,
                "layoutbox",
                -- cpu_icon,
                -- cpuwidget,
                "promptbox",
            },
            { -- Middle widget
                layout = wibox.layout.fixed.horizontal,
                {
                    name = "clock",
                    settings = "%H:%M"
                },
            },
            { -- Right widgets
                layout = wibox.layout.fixed.horizontal,
                "system-tray",
                --[[{
                    name = "volume",
                    settings = {
                        widget_type = 'arc',
                        device = "default",
                    }
                },
                'logout-menu',
                ]]--

            },
        }
    }

    -- Describe bottom panels
    local bottom_panel = {
        params = {
            bg = beautiful.bg_normal,
            position = "bottom",
            size = 25, 
            -- size_activator = 1
            show_delay = 0.25,
            hide_delay = 0.5,
            easing = 2,
            delta = 5,

            screen = s,
            slide = true
        },
        setup = {
            layout = wibox.layout.align.horizontal,
            { -- Left widgets
                layout = wibox.layout.fixed.horizontal,
                mylauncher,
                'taglist',
            },
            { -- Middle widget
                layout = wibox.layout.fixed.horizontal,
                'tasklist'
            },
            { -- Right widgets
                layout = wibox.layout.fixed.horizontal,
                "keyboard-layout",
            },
        }
    }

    return {
        top_panel,
        bottom_panel,
    }
end
