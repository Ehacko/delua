-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")
pcall(require, "user")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

if User == nil then require("delua.user") end
-- This is used later as the default terminal and editor to run.
apps = {}
apps.terminal= "x-terminal-emulator"
editor = os.getenv("EDITOR") or "editor"
editor_cmd = apps.terminal.. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- {{{ Error handling
    require("delua.errors")(naughty)
-- }}}

-- {{{ start execution
    require("delua.startup")(awful)(User.startup)
-- }}}

-- {{{ Variable definitions
    -- Themes define colours, icons, font and wallpapers.
    -- beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
    beautiful.init(require("delua.themes"))

-- }}}

-- {{{ 
    awful.screen.connect_for_each_screen(require("delua.screens")(awful, beautiful, gears, hotkeys_popup, wibox))
-- }}}

-- {{{ 
    require("delua.controls")(awful, gears, hotkeys_popup)
--}}}

-- {{{ Rules
    require("delua.rules")(awful, beautiful)
-- }}}

-- {{{ Signals
    require("delua.signals")(awful, beautiful, gears, wibox)
-- }}}

return true
