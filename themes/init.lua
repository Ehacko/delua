local dpi   = require("beautiful.xresources").apply_dpi
local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()


local theme                                     = {}

-- {{{ Général }}}
-- {{{ Titlebars }}}
-- {{{ Panels }}}

theme.default_dir                               = require("awful.util").get_themes_dir() .. "default"
theme.icon_dir                                  = os.getenv("HOME") .. "/.config/awesome/themes/custom/icons"
theme.wallpaper                                 = os.getenv("HOME") .. "/.config/awesome/wallpapers/car.jpeg"
-- theme.wallpaper                                 = os.getenv("HOME") .. "/.config/awesome/themes/custom/wall.png"
theme.font                                      = "Roboto Bold 10"
theme.taglist_font                              = "Roboto Condensed Regular 8"
theme.fg_normal                                 = "#DDDDDD"
theme.titlebar_fg_normal                        = "#454545"
theme.titlebar_fg_focus                         = "#BBDDFF"
theme.fg_focus                                  = "#454545"
theme.titlebar_bg_focus                         = "#454545EE"
theme.bg_focus                                  = "#DDDDDDEE"
theme.bg_normal                                 = "#454545CC"
theme.titlebar_bg_normal                        = "#DDDDDDCC"
theme.fg_urgent                                 = "#CC9393"
theme.bg_urgent                                 = "#006B8E"
theme.border_width                              = dpi(1.25)
theme.border_normal                             = "#252525"
theme.border_focus                              = "#BBDDFF"
theme.taglist_fg_focus                          = "#DDDDDD"
theme.tasklist_bg_normal                        = "#22222200"
theme.tasklist_fg_focus                         = "#454545"
theme.tasklist_bg_focus                         = "#BBDDFF75"
theme.tasklist_bg_urgent                        = "#DD454575"
theme.menu_height                               = dpi(20)
theme.menu_width                                = dpi(160)
theme.menu_icon_size                            = dpi(32)
theme.awesome_icon                              = require("beautiful.theme_assets").awesome_icon(theme.menu_height, theme.bg_focus, theme.fg_focus)
theme.awesome_icon_launcher                     = theme.awesome_icon
theme.taglist_squares_sel                       = theme.icon_dir .. "/square_sel.png"
theme.taglist_squares_unsel                     = theme.icon_dir .. "/square_unsel.png"
theme.spr_small                                 = theme.icon_dir .. "/spr_small.png"
theme.spr_very_small                            = theme.icon_dir .. "/spr_very_small.png"
theme.spr_right                                 = theme.icon_dir .. "/spr_right.png"
theme.spr_bottom_right                          = theme.icon_dir .. "/spr_bottom_right.png"
theme.spr_left                                  = theme.icon_dir .. "/spr_left.png"
theme.bar                                       = theme.icon_dir .. "/bar.png"
theme.bottom_bar                                = theme.icon_dir .. "/bottom_bar.png"
theme.mpdl                                      = theme.icon_dir .. "/mpd.png"
theme.mpd_on                                    = theme.icon_dir .. "/mpd_on.png"
theme.prev                                      = theme.icon_dir .. "/prev.png"
theme.nex                                       = theme.icon_dir .. "/next.png"
theme.stop                                      = theme.icon_dir .. "/stop.png"
theme.pause                                     = theme.icon_dir .. "/pause.png"
theme.play                                      = theme.icon_dir .. "/play.png"
theme.clock                                     = theme.icon_dir .. "/clock.png"
theme.calendar                                  = theme.icon_dir .. "/cal.png"
theme.cpu                                       = theme.icon_dir .. "/cpu.png"
theme.net_up                                    = theme.icon_dir .. "/net_up.png"
theme.net_down                                  = theme.icon_dir .. "/net_down.png"
theme.layout_fairh                              = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv                              = themes_path.."default/layouts/fairvw.png"
theme.layout_floating                           = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier                          = themes_path.."default/layouts/magnifierw.png"
theme.layout_max                                = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen                         = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom                         = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft                           = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile                               = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop                            = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral                             = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle                            = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw                           = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne                           = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw                           = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse                           = themes_path.."default/layouts/cornersew.png"
theme.tasklist_plain_task_name                  = true
theme.tasklist_disable_icon                     = true
theme.useless_gap                               = dpi(2.5)

theme.titlebar_close_button_normal              = theme.default_dir.."/titlebar/close_normal.png"
theme.titlebar_close_button_focus               = theme.default_dir.."/titlebar/close_focus.png"
theme.titlebar_minimize_button_normal           = theme.default_dir.."/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus            = theme.default_dir.."/titlebar/minimize_focus.png"
theme.titlebar_ontop_button_normal_inactive     = theme.default_dir.."/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive      = theme.default_dir.."/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active       = theme.default_dir.."/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active        = theme.default_dir.."/titlebar/ontop_focus_active.png"
theme.titlebar_sticky_button_normal_inactive    = theme.default_dir.."/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive     = theme.default_dir.."/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active      = theme.default_dir.."/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active       = theme.default_dir.."/titlebar/sticky_focus_active.png"
theme.titlebar_floating_button_normal_inactive  = theme.default_dir.."/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive   = theme.default_dir.."/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active    = theme.default_dir.."/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active     = theme.default_dir.."/titlebar/floating_focus_active.png"
theme.titlebar_maximized_button_normal_inactive = theme.default_dir.."/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme.default_dir.."/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active   = theme.default_dir.."/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active    = theme.default_dir.."/titlebar/maximized_focus_active.png"

return theme