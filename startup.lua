local function Auto_start(awful)

    return function(cmd_arr)

        for _, cmd in ipairs(cmd_arr) do
            awful.spawn.with_shell(string.format("pgrep -u $USER -fx '%s' > /dev/null || (%s)", cmd, cmd))
        end
        
    end
    
end

return Auto_start