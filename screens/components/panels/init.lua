local function Panels(awful, beautiful, gears, hotkeys_popup, wibox)

    -- {{{ Wibar

    local add_applets = require("delua.screens.components.panels.applets")(awful, gears, wibox)
    
    local function new_panel(panel, s)
        local current_panel
        -- Create the wibox
        if panel.params.slide == true then
            current_panel = require("delua.screens.components.panels.slidebar")(panel.params)
        else
            current_panel = awful.wibar(panel.params)
        end

        -- Add applets to the wibox
        local setup = {
            layout = panel.setup.layout,
            expand = panel.setup.expand,
        }
        for i=1, 3 do
            setup[i] = add_applets(panel.setup[i], s)
        end

        current_panel:setup (setup)

        return current_panel

    end

    return function(s)
        
        -- génère les panels à partir des données de user.config
        local panels = User.panels(beautiful, s, wibox)
        if s.panels == nil then s.panels = {} end
        for i=1, #(panels) do
            s.panels[#(s.panels)] = new_panel(panels[i],s)
        end
        
    end

end

return Panels
