local function Applets(awful, gears, wibox)
    
    local path = "delua.screens/components/panels/applets/%s"
    local dir = gears.filesystem.get_dir ("config") .. path .. "/init.lua"

    local function get_applet(applet_name)
        return require(string.format(path, applet_name))(awful, gears, wibox)
    end

    local function file_exists(name)
        local f=io.open(name,"r")
        if f~=nil then io.close(f) return true else return false end 
    end

    return function (applets, s)

        local function new_applet(applet, my_args)
            
            if type(applet) ~= "string" then
                return applet
            elseif file_exists(string.format(dir, applet)) then
                return get_applet(applet)(my_args)
            end
            -- return nil
        end
        
        local function add_applet(applet)
            
            if type(applet) == "table" and type(applet.name) == "string" then
                return new_applet(applet.name, applet.settings)
            else
                return new_applet(applet, s)
            end

        end

        local applets_list = {}


        if type(applets) ~= "table" then
            applets_list = add_applet(applets)
        else
            for k, v in pairs(applets) do

                applets_list[k] =  (k == "layout") and v or add_applet(v)

            end
        end

        return applets_list
    end

end

return Applets
