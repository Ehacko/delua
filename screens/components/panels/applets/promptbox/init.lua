local function Promptboxes(awful)
    
    -- Create a promptbox for each screen
    return function ()
        return awful.widget.prompt()
    end

end

return Promptboxes