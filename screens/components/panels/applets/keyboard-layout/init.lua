local function Keyboardlayout(awful)
    return function(s)
        return awful.widget.keyboardlayout()
    end
end

return Keyboardlayout