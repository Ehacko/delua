local function Clocks(awful, gears, wibox)
    return function(args)
        return wibox.widget.textclock(args)
    end
end

return Clocks