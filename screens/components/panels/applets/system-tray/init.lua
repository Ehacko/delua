local function Tray(awful, gears, wibox)
    return function(args)
        return wibox.widget.systray(args)
    end
end

return Tray