-- Load Debian menu entries
local menubar = require("menubar")
local debian = pcall(require, "debian.menu")
local has_fdo, freedesktop = pcall(require, "freedesktop")

local function Menus(awful, beautiful)
    -- Create a launcher widget and a main menu
    myawesomemenu = {
        { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
        { "manual", apps.terminal.. " -e man awesome" },
        { "edit config", editor_cmd .. " " .. awesome.conffile },
        { "restart", awesome.restart },
        { "quit", function() awesome.quit() end },
    }

    local menu_awesome = { "awesome", myawesomemenu, beautiful.awesome_icon }
    local menu_terminal = { "open terminal", apps.terminal}

    if has_fdo then
        mymainmenu = freedesktop.menu.build({
            before = { menu_awesome },
            after =  { menu_terminal }
        })
    elseif debian then
        mymainmenu = awful.menu({
            items = {
                    menu_awesome,
                    { "Debian", debian.menu.Debian_menu.Debian },
                    menu_terminal,
                    }
        })
    else
        mymainmenu = "rofi -show drun"
    end


    mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })

    -- Menubar configuration
    menubar.utils.terminal = apps.terminal-- Set the terminal for applications that require it
end

return Menus
