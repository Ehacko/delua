local function Screens(awful, beautiful, gears, hotkeys_popup, wibox)
    local add_panels = require("delua.screens.components.panels")(awful, beautiful, gears, hotkeys_popup, wibox)
    -- {{{ Layouts
        -- Table of layouts to cover with awful.layout.inc, order matters.
        require("delua.screens.components.layouts")(awful)
    -- }}}

    -- {{{ Menu
        require("delua.screens.components.menus")(awful, beautiful)
    -- }}}

    local function set_wallpaper(s)
        -- Wallpaper
        if beautiful.wallpaper then
            local wallpaper = beautiful.wallpaper
            -- If wallpaper is a function, call it with the screen
            if type(wallpaper) == "function" then
                wallpaper = wallpaper(s)
            end
            gears.wallpaper.maximized(wallpaper, s, true)
        end
    end
    
    -- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
    -- screen.connect_signal("property::geometry", set_wallpaper)
    return function(s)
        
        -- Wallpaper
        set_wallpaper(s)
        
        -- Each screen has its own tag table.
        awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])
        
        add_panels(s)
        
    end
    
end
return Screens
